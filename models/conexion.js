const mysql = require("mysql2");
const { config } = require("dotenv");

config();

const connection = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "admin12345",
    database: "sistema",
})

connection.connect( (err) => {
    if (err) {
        console.log("Surgio un error al conectarse" + err);
    } else {
        console.log("Se conectó con exito")
    }
})

connection.query('SELECT * from alumno', function (error, results, fields) {
    if (error)
        throw error;

    results.forEach(result => {
        console.log(result);
    });


});
module.exports = connection;