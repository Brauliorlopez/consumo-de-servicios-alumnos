"use strict";
const form = document.querySelector("#form");
const btnActualizar = document.querySelector("#btnActualizar");

function showAlert(message, title) {
	const modalToggle = document.getElementById("alertModal");
	const myModal = new bootstrap.Modal("#alertModal", { keyboard: false });
	document.getElementById("alertTitle").innerHTML = title;
	document.getElementById("alertMessage").innerHTML = message;
	myModal.show(modalToggle);
}
const getInputs = () => {
	return {
		id: form['id'].value.trim(),
		matricula: form['matricula'].value.trim(),
		nombre: form['nombre'].value.trim(),
		domicilio: form['domicilio'].value.trim(),
		sexo: form['sexo'].value.trim(),
		especialidad: form['especialidad'].value.trim(),
	};
};

async function updateAlumno(event) {
	try {
		event.preventDefault();
		let { matricula, nombre, domicilio, sexo, especialidad } = getInputs();

		// Validación de campos
		if (!matricula || !nombre || !domicilio || !sexo || !especialidad) return showAlert("","*Todos los campos son requeridos");

		// Creación de objeto a mandar petición
		let alumnoModificado = {
			matricula,
			nombre,
			domicilio,
			sexo,
			especialidad,
		}

		// Petición de actualizacion de registro sin imagen
		await axios.post(window.location.pathname, alumnoModificado, {
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		});

		showAlert("","*Se actualizó con exito");

		setTimeout(() => {
			window.location.href = '/';
		}, 2000);

	} catch (error) {
		showAlert(error.response.data, "Error");
	}
}

// Creacion de escuchadores de eventos
btnActualizar.addEventListener("click", updateAlumno)
form.addEventListener("reset", (event) => {
	document.querySelector("#imgPreview").src = ulrImagenDefault;
})